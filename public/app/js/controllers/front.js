(function(){

		var app	=	angular.module('app',['ngRoute','ngAnimate','ngQuickDate']);

       app.config(function(ngQuickDateDefaultsProvider) {
        return ngQuickDateDefaultsProvider.set({
                closeButtonHtml: "<i class='fa fa-times'></i>",
                buttonIconHtml: "<i class='fa fa-calendar'></i>",
                nextLinkHtml: "<i class='fa fa-chevron-right'></i>",
                prevLinkHtml: "<i class='fa fa-chevron-left'></i>"
        });
      });

		
		/** 		Controlador Principal 			**/
		app.controller('GaleriaController',['$scope','$location','$routeParams','$http','$timeout',function($scope,$location,$routeParams,$http,$timeout){
        $scope.fotos  =  x;
        $scope.galeriaInfo = galInfo;
        $scope.loading=0;

        $scope.nombreGaleria = $routeParams.nombre;
        $scope.idGaleria  =   $routeParams.idgaleria;

          
        $scope.verFoto = function(indice){
          $scope.loading=1;
          $scope.idFotoActual = $scope.fotos[indice];
          
        }

        $scope.verFoto(0);

            

		}]);


    app.controller('FormularioCtrl', ['$scope','$http', function ($scope,$http) {
        $scope.solicitud = {};
        $scope.success = 0;
        $scope.enviarSolicitud = function(){
            $scope.success = 0;
            $("#loadingModal").modal("show");
            var request = $http({
                              method: "post",
                              url: "/default/index/peticion",
                              data: $scope.solicitud
                          });

              request.success(function(data, status, headers, config) {
                  $scope.solicitud = {}; //limpiamos la entrada
                  $scope.success = 1;
                  $("#loadingModal").modal("hide");
              })
              .error(function(data,status,headers,config){
                  $("#loadingModal").modal("hide");
              });

        }
      
    }]);


          app.directive('imageonload', function($timeout) {
              return {
                  restrict: 'A',
                
                  link: function(scope, element,timeout) {
                        element.on('load', function() {
                          $timeout(function() {
                            scope.loading=0;
                            scope.$apply();
                          },0);
                        $('#animacion').removeClass().addClass('flipInY animated')
                          .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                            $(this).removeClass();
                          });
                        
                    });
                    scope.$watch('ngSrc', function() {
                       $timeout(function() {
                        scope.loading=1;
                        scope.$apply();
                      },0);
                    });
                  }
              };
          });    



	
})();