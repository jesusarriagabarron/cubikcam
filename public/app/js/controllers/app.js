function showError(mensaje){
  $("#loadingModal").modal("hide");
    $.pnotify({
                        title:    'Error',
                        text:     mensaje,
                        type:     'error', 
                        closer:   true,
                        sticker:  false,
                        delay:    3000
                    });
}

function showSuccess(mensaje){
  $("#loadingModal").modal("hide");
    $.pnotify({
                        title:    '¡Muy bien!',
                        text:     mensaje,
                        type:     'success', 
                        closer:   true,
                        sticker:  false,
                        delay:    3000
                    });
}



(function(){

		var app	=	angular.module('app',['ngRoute','ngAnimate','galeriaModule']).
		config(['$routeProvider', function($routeProvider) {
  				$routeProvider.
  					when("/", {
                        templateUrl: "/app/html/dashboard.html"
                }).
            when("/mostrar-galerias",{
                              templateUrl:"/app/html/galerias.html",
                              controller : "MostrarGaleriaController"
                           }).
            when("/crear-galeria",{
                              templateUrl:"/app/html/crear-galeria.html",
                              controller : "CrearGaleriaController"
                           }).
            when("/galeria/:idGaleria",{
                              templateUrl:"/app/html/galeria.html",
                              controller : "MostrarUnaGaleriaController"
                           }).
  					otherwise({redirectTo: '/'});
		}]);
		
		/** 		Controlador Principal 			**/
		app.controller('MainController',['$scope','$location','$route','$http',function($scope,$location,$route,$http){
			$scope.$route = $route;
     		$scope.$location = $location;
     		this.usuario = [];

     		$scope.getModule = function(){
     				ubicacion	=	$location.path();
     				var res = ubicacion.split("/");
     				return res[1];
     		};

     		
		}]);
	
})();