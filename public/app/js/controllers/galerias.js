        
(function(){

	var app 	=	angular.module('galeriaModule', ['angularFileUpload','ngQuickDate','servicios']);

    app.config(function(ngQuickDateDefaultsProvider) {
        return ngQuickDateDefaultsProvider.set({
                closeButtonHtml: "<i class='fa fa-times'></i>",
                buttonIconHtml: "<i class='fa fa-calendar'></i>",
                nextLinkHtml: "<i class='fa fa-chevron-right'></i>",
                prevLinkHtml: "<i class='fa fa-chevron-left'></i>"
        });
      });




    /** Controlador para crear una galeria **/
	app.controller('CrearGaleriaController', ['$scope','$location','FileUploader','ApiGaleria', function ($scope,$location,FileUploader,ApiGaleria) {
        $scope.galeria = [];
        $scope.setToToday = function() {  $scope.galeria.fechaEvento = new Date();    }
        //Enviar datos de la galeria !
        $scope.crearGaleria = function(galeriaNueva){ 
                                    var galeria = new ApiGaleria();
                                    galeria.nombre      = galeriaNueva.nombre;
                                    galeria.fechaEvento = galeriaNueva.fechaEvento;
                                    ApiGaleria.save(galeria,
                                                    function(data){
                                                    $scope.galeria.id = data.response.idGaleria;
                                                    },
                                                    function(error){
                                                        console.log(error)
                                                    });
                                };
		var uploader 	=	$scope.uploader 	=	new FileUploader({
			 url: '/admin/panel/upload'
		});

		 uploader.filters.push({
            name: 'imageFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });

        uploader.onBeforeUploadItem = function(item) {
            item.formData.push({idGaleria:$scope.galeria.id});
        };
        uploader.onCompleteAll = function() {
            $scope.galeria.completada = 1;
            $location.path("/galeria/"+$scope.galeria.id); 
        };

	}]);
	



    /**     Controlador para mostrar las galerias    **/
	app.controller('MostrarGaleriaController', ['$scope','ApiGaleria', function ($scope,ApiGaleria) {
            $("#loadingModal").modal("show");
            $scope.galerias = [];
            $scope.galerias = ApiGaleria.query(
                                                function(){$("#loadingModal").modal("hide");},
                                                function(error){ showError("No se encontró  la galeria");});

	}]);




    /**     Controlador para mostrar una galeria especifica     **/
    app.controller('MostrarUnaGaleriaController', ['$scope','$routeParams','ApiGaleria', function ($scope,$routeParams,ApiGaleria) {
            $("#loadingModal").modal("show");
            $scope.galeria = [];
            $scope.params = $routeParams;
            $scope.galeria = ApiGaleria.get({idGaleria:$scope.params.idGaleria},
                                            function(){$("#loadingModal").modal("hide");},
                                            function(error){ showError("No se encontró  la galeria");});

            $scope.cambiarStatus    =   function(){
                                                var statusAnterior = $scope.galeria.status;

                                                if($scope.galeria.status)
                                                    $scope.galeria.status=0;
                                                else
                                                    $scope.galeria.status=1;

                                               ApiGaleria.update($scope.galeria,
                                                function(data) {showSuccess("Se ha cambiado el status de la galeria correctamente")},
                                                function(error){showError("No se pudo cambiar el status de la galeria");
                                                                $scope.galeria.status=statusAnterior;});
                                        }
            

    }]);





    app.controller('EditarGaleriaController', ['$scope', function ($scope) {
        

    }]);

})();