var lon   = -99.1338228;
var lat   = 19.4335777;
var marker  = null;
var marker2 = null;
var image = '/images/hospital.png';
var map;
var map2;
var latlng ;



function gmap_default_init2(lat,lon) {

    if(marker2){
          marker2.setMap(null);
    }

    latlng = new google.maps.LatLng(lat,lon);
    var mapOptions = {
      zoom: 14,
      center: latlng
    };

   map2 = new google.maps.Map(document.getElementById('gmap-default2'), mapOptions);

   marker2 = new google.maps.Marker({
        position: latlng,
        map: map2,
        icon:image
    });
}



function gmap_default_init() {
    var mapOptions = {
      zoom: 14,
      center: new google.maps.LatLng(lat, lon)
    };

   map = new google.maps.Map(document.getElementById('gmap-default'), mapOptions);

  
  function placeMarker(location) {
    
     marker = new google.maps.Marker({
        position: location,
        map: map,
        icon:image,
        draggable:true,
    });

      if(marker)
      google.maps.event.addListener(marker, 'dragend', function() 
      {
           $("#latitud").val(marker.getPosition().lat());
            $("#longitud").val(marker.getPosition().lng());
      });
      $("#latitud").val(marker.getPosition().lat());
      $("#longitud").val(marker.getPosition().lng());
     
  };


  google.maps.event.addListener(map, 'click', function(event) {
        if(marker)
          marker.setMap(null);

      placeMarker(event.latLng);
  });

  geocoder = new google.maps.Geocoder();

    function codeAddress() {
        var address = document.getElementById("inputDireccion").value;
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                if(marker)
                  marker.setMap(null);
                placeMarker(results[0].geometry.location);

            } else {
        
            }
        });
    }

    $('#inputDireccion').keypress(function(event){
        codeAddress();
    });
   
}

function gmap_initialize() {
     if (navigator.geolocation)
      {
        navigator.geolocation.getCurrentPosition(
          function(pos){
            lon   =   pos.coords.longitude;
            lat   =   pos.coords.latitude;
            gmap_default_init();
            gmap_default_init2();
          }
        );
      }
      else
      {
        
      };
  
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=weather&sensor=false&' + 'callback=gmap_initialize';
  document.body.appendChild(script);
}

$(function() {
  // GOOGLE MAPS
  if($('#gmap-default').length){
    loadScript();
  }
});