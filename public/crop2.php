<?php

defined('CRON_PATH')
|| define('CRON_PATH', realpath(dirname(__FILE__) ));
	

// validamos que la ruta no contenga ../ defensa contra directory traversal
if(!isset($_GET['img']) || substr_count($_GET['img'],'..') > 0){
    exit;
}	
	
$dominiosValidos = array('cubikcam.com','www.cubikcam.com','cubikcam.local');	
$imagenValida=0;
$imagen = $_GET['img'];

$dominio = $_SERVER["SERVER_NAME"];

foreach ($dominiosValidos as $k => $v){
	if(substr_count($dominio,$v)>0)
		$imagenValida=1;
}




if(!$imagenValida)
	die(".");

$width = @$_GET['width'];
if(!is_numeric($width))
	$width="";

$height = @$_GET['height'];
if(!is_numeric($height))
	$height="";

	
$coordinates=@$_GET['coordinates'];
if(!preg_match("/^([1-9]|[1-9][0-9])" ."(,([1-9]|[1-9][0-9]))$/", $coordinates))
	$coordinates="50,50";


include_once(CRON_PATH."/ImageManipulator.php");

/*       PARAMETROS CONFIGURACION             */

// path en donde se guardan las imagenes resampleadas
$imgResampledFolder = '/home/jarriaga/git/images/galerias/resamp';
// nombre de la imagen a regresar
$vector = explode(",", $coordinates);
$imgResampledPath = $imgResampledFolder.'/'.md5($imagen.'seed').'_'.$width.'_x_'.$height.'_focus_'.$vector[0]."_x_".$vector[1];

/*
if(file_exists($imgResampledPath)){// file_exists($imgResampledPath)
    $im = new ImageManipulator($imgResampledPath);
    $im->imprimir();
    exit;
}
*/

/* 		FIN PARAMS CONFIGURACION 			  */
$imagen =  CRON_PATH."/../galerias/".$imagen;
//var_dump($image);exit;


$im = new ImageManipulator($imagen);


//Obtengo las dimensiones de la imagen
$imWidth = $im->getWidth();
$imHeight = $im->getHeight();


if(!$width||!$height)
{
	if($width){
		$resizeWidth = $width; 
		$proporcion = $resizeWidth*100/$imWidth;
		$resizeHeight = round($proporcion*$imHeight/100);
		$im->resample($resizeWidth, $resizeHeight);
	}
	else{
		if($height){
			$resizeHeight = $height; 
			$proporcion = $resizeHeight*100/$imHeight;
			$resizeWidth = round($proporcion*$imWidth/100);
			$im->resample($resizeWidth, $resizeHeight);
		}
	}
}
else{
/*       PARAMETROS CONFIGURACION             */

//Defino si el crop er horizontal o vertical
if($width>$height)
	$crophorizontal=1;
else
	$crophorizontal=0;

//Defino si la imagen es horizontal o vertical
if($imWidth>$imHeight)
	$imHorizontal=1;
else
	$imHorizontal=0;
	
if ($imHorizontal){
	//Imagen Horizontal
	if($crophorizontal){
		
		//Imagen Horizontal y Crop Horizontal
		//resize hasta el vertical
		
		$resizeWidth = $width; 
		$proporcion = $resizeWidth*100/$imWidth;
		$resizeHeight = round($proporcion*$imHeight/100);
		$im->resample($resizeWidth, $resizeHeight);

		$midwidth = round($width/2);
		$midheight = round($height/2);
		
		$vector = explode(",", $coordinates);
		
		$centreX = round(($vector[0]*$resizeWidth)/100);
		$centreY = round(($vector[1]*$resizeHeight)/100);
			
		$x1 = $centreX - $midwidth;
		$y1 = $centreY - $midheight;
		
		$x2 = $centreX + $midwidth;
		$y2 = $centreY + $midheight;
		
		if($resizeHeight>$height){
			//crop xq se sale la imagen
			if($y1<0){
				$y2=$y2-$y1;
				$y1=0;
			}
			else{
				if($y2>$resizeHeight){
					$y2=$resizeHeight;
					$y1=$resizeHeight-$height;
				}
			}
			$im->crop(0,$y1,$width,$y2);			
		}
		else{
			
			if($resizeHeight<$height){
				if($imHeight>=$height){
					
					//nuevo resize
					$resizeHeight = $height; 
					$proporcion = $resizeHeight*100/$imHeight;
					$resizeWidth = round($proporcion*$imWidth/100);
					$im = new ImageManipulator($imagen);
					$im->resample($resizeWidth, $resizeHeight);
					if($resizeWidth>$width){
						//crop
						$centreX = round(($vector[0]*$resizeWidth)/100);
						$centreY = round(($vector[1]*$resizeHeight)/100);
							
						$x1 = $centreX - $midwidth;
						$y1 = $centreY - $midheight;
						
						$x2 = $centreX + $midwidth;
						$y2 = $centreY + $midheight;
						
						if($x1<0){
							$x2=$x2-$x1;
							$x1=0;
						}
						else{
							if($x2>$resizeWidth){
								$x2=$resizeWidth;
								$x1=$resizeWidth-$width;
							}
						}
						$im->crop($x1,0,$x2,$height);
					}
					else{
						$im->enlargeCanvas($width, $height);
					}
				}
				else{
					$im->enlargeCanvas($width, $height);
				}
			}
		}
	}
	else{
		//Img horizontal crop vertical
		$resizeHeight = $height; 
		$proporcion = $resizeHeight*100/$imHeight;
		$resizeWidth = round($proporcion*$imWidth/100);
		$im->resample($resizeWidth, $resizeHeight);
		
		$midwidth = round($width/2);
		$midheight = round($height/2);
		
		$vector = explode(",", $coordinates);
		
		$centreX = round(($vector[0]*$resizeWidth)/100);
		$centreY = round(($vector[1]*$resizeHeight)/100);
			
		$x1 = $centreX - $midwidth;
		$y1 = $centreY - $midheight;
		
		$x2 = $centreX + $midwidth;
		$y2 = $centreY + $midheight;
		
		if($x1<0){
			//desde 0 hasta el max
			$x1=0;
			$x2=$width;
			$im->crop($x1, 0, $x2, $resizeHeight);
		}else{
			if($x2>$resizeWidth){
				$x1=$resizeWidth-$width;
				$x2=$resizeWidth;
				$im->crop($x1, 0, $x2, $resizeHeight);
				//desde el max menos el x 
				
			}
			else{
				//normal
				$im->crop($x1, 0, $x2, $resizeHeight); // takes care of out of boundary conditions automatically
			}
		}
	}
}
else{
	//Imagen Vertical
	if($crophorizontal){
		//Img vertical crop horizontal
		$resizeWidth = $width; 
		$proporcion = $resizeWidth*100/$imWidth;
		$resizeHeight = round($proporcion*$imHeight/100);
		$im->resample($resizeWidth, $resizeHeight);
		
		$midwidth = round($width/2);
		$midheight = round($height/2);
		
		$vector = explode(",", $coordinates);
		
		$centreX = round(($vector[0]*$resizeWidth)/100);
		$centreY = round(($vector[1]*$resizeHeight)/100);
			
		$x1 = $centreX - $midwidth;
		$y1 = $centreY - $midheight;
		
		$x2 = $centreX + $midwidth;
		$y2 = $centreY + $midheight;
		
		if($y1<0){
			//desde 0 hasta el max
			$y1=0;
			$y2=$height;
			$im->crop(0, $y1, $resizeWidth, $y2);
		}else{
			if($y2>$resizeHeight){
				$y1=$resizeHeight-$height;
				$y2=$resizeHeight;
				$im->crop(0, $y1, $resizeWidth, $y2);
				//desde el max menos el x 
				
			}
			else{
				//normal
				$im->crop(0, $y1, $resizeWidth, $y2); // takes care of out of boundary conditions automatically
			}
		}
		
	}
	else{
		//Imagen Vertical y Crop Vertical
		$resizeHeight = $height; 
		$proporcion = $resizeHeight*100/$imHeight;
		$resizeWidth = round($proporcion*$imWidth/100);
		$im->resample($resizeWidth, $resizeHeight);

		$midwidth = round($width/2);
		$midheight = round($height/2);
		
		$vector = explode(",", $coordinates);
		
		$centreX = round(($vector[0]*$resizeWidth)/100);
		$centreY = round(($vector[1]*$resizeHeight)/100);
			
		$x1 = $centreX - $midwidth;
		$y1 = $centreY - $midheight;
		
		$x2 = $centreX + $midwidth;
		$y2 = $centreY + $midheight;
		
		if($resizeWidth>$width){
			//crop xq se sale la imagen
			if($x1<0){
				$x2=$x2-$x1;
				$x1=0;
			}
			else{
				if($x2>$resizeWidth){
					$x2=$resizeWidth;
					$x1=$resizeWidth-$width;
				}
			}
			$im->crop($x1,0,$x2,$height);			
		}
		else{
			
			if($resizeWidth<$width){
				
				if($imWidth>=$width){
					//nuevo resize
					$resizeWidth = $width; 
					$proporcion = $resizeWidth*100/$imWidth;
					$resizeHeight = round($proporcion*$imHeight/100);
					$im = new ImageManipulator($imagen);
					$im->resample($resizeWidth, $resizeHeight);
					if($resizeHeight>$height){
						//crop
						$centreX = round(($vector[0]*$resizeWidth)/100);
						$centreY = round(($vector[1]*$resizeHeight)/100);
							
						$x1 = $centreX - $midwidth;
						$y1 = $centreY - $midheight;
						
						$x2 = $centreX + $midwidth;
						$y2 = $centreY + $midheight;
						
						if($y1<0){
							$y2=$y2-$y1;
							$y1=0;
						}
						else{
							if($y2>$resizeHeight){
								$y2=$resizeHeight;
								$y1=$resizeHeight-$height;
							}
						}
						$im->crop(0,$y1,$width,$y2);
					}
					else{
						$im->enlargeCanvas($width, $height);
					}
				}
				else{
					$im->enlargeCanvas($width, $height);
				}
			}
		}
	}
}
}



	function getRequestHeaders() 
				{ 
				    if (function_exists("apache_request_headers")) 
				    { 
				        if($headers = apache_request_headers()) 
				        { 
				            return $headers; 
				        } 
				    } 

				    $headers = array(); 
				    // Grab the IF_MODIFIED_SINCE header 
				    if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) 
				    { 
				        $headers['If-Modified-Since'] = $_SERVER['HTTP_IF_MODIFIED_SINCE']; 
				    } 
				    return $headers; 
				}

				// Return the requested graphic file to the browser 
				// or a 304 code to use the cached browser copy 
				function cache ($graphicFileName, $fileType='jpeg') 
				{ 
				    $fileModTime = filemtime($graphicFileName); 
				    // Getting headers sent by the client. 
				    $headers = getRequestHeaders(); 
				    // Checking if the client is validating his cache and if it is current. 
				    if (isset($headers['If-Modified-Since']) && 
				        (strtotime($headers['If-Modified-Since']) == $fileModTime)) 
				    { 
				        // Client's cache IS current, so we just respond '304 Not Modified'. 
				        header('Last-Modified: '.gmdate('D, d M Y H:i:s', $fileModTime).
				                ' GMT', true, 304); 
				    } 
				    else 
				    { 
				        // Image not cached or cache outdated, we respond '200 OK' and output the image. */
				       header('Last-Modified: '.gmdate('D, d M Y H:i:s', $fileModTime).
				              ' GMT', true, 200); 
				    	header("Cache-Control: private, max-age=10800, pre-check=10800");
						header("Pragma: private");
						header("Expires: " . date(DATE_RFC822,strtotime("30 day")));
				        header('Content-type: image/'.$fileType); 
				        header('Content-transfer-encoding: binary'); 
				        header('Content-length: '.filesize($graphicFileName)); 
				   } 
				} 

cache($imagen);
$im->imprimir();
//$im->save($imgResampledPath);

?>
