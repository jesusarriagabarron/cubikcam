<?php
/**
 * Clase para controlar el panel de administracion
 * subida de archivos con upload
 * @author jarriaga
 *
 */


class admin_PanelController extends My_Controller_Action
{
		public function indexAction(){

		}


		/** @author jarriaga
		*	Metodo que controla el manejo de subida de archivos jpg para las galerias
		*/
		public function uploadAction(){
			//deshabilitamos los layouts
			 $this->_helper->layout()->disableLayout(); 	
			//comprobamos que tengamos archivos
			if ( !empty( $_FILES ) ) {

					$idGaleria 	= (int)	$this->getRequest()->getParam("idGaleria");

					$galeria 	=	$this->_em->find("Default_Model_Galeria",$idGaleria);



					if(!$galeria){
						$this->getResponse()->setHttpResponseCode(500);
					 	$answer = array("error"=>500);
				     	$this->_helper->json->sendJson($answer);
					}


					//validadores
					$upload = new Zend_File_Transfer();
					$upload->addValidator('Size', false, 1024*1024*5);
					$upload->addValidator('Extension', false, array('jpg', 'png','jpeg'));
					$upload->addValidator('IsImage', false);
					
					//si no es valido a la goma				    
				    if (!$upload->isValid()) {
				        $answer = array( 'answer' => 'No son validos los archivos' );
						$this->_helper->json->sendJson($answer);

				    }else{
				    	//si es valido lo guardamos en "galerias"
				    	$upload->addFilter('Rename', array(
								'target' => APPLICATION_PATH.'/../galerias/'.$upload->getFileName(null,false),
								'overwrite' => true
						));
				    			//si no se pudo recibir a la goma
								if (!$upload->receive()) {
									$messages = $upload->getMessages();
									$this->_helper->json->sendJson($messages);

								} else {
									//crear la fotografia y enlazarla a la galeria
									$nombreArchivo = $upload->getFileName(null,false);
									$foto 	=	new Default_Model_Foto();
									$foto->setFilename($nombreArchivo);
									$foto->setGaleria($galeria);
									$this->_em->persist($foto);
									$this->_em->flush();

									//si se recibio avisamos
									$answer = array( 'answer' => 'File transfer completed' );
									$this->_helper->json->sendJson($answer);

								}
				    }
				} else {
					//no fue valido a la goma
					 $this->getResponse()->setHttpResponseCode(500);
					 $answer = array("error"=>500);
				     $this->_helper->json->sendJson($answer);
				}
		}
}