<?php
/**
 * Modelo para controlar el Home y el login de los usuarios
 * @author jarriaga
 *
 */


class admin_IndexController extends My_Controller_Action
{
		public function indexAction(){
			$request = $this->getRequest();
			$this->view->error ="";

			if($request->isPost()){
				$username 	=	filter_var($request->getParam("username"),FILTER_SANITIZE_EMAIL);
				$password	=	$request->getParam("password");

				$usuarioModel 	=	new My_Model_Usuario();

				$result	=	$usuarioModel->userLogin($username,$password);
				if($result){
					$this->_redirect("/admin/panel");
				}else{
					$this->view->error = "Usuario/Password incorrecto";
				}

			}


		}

}