<?php

/**
 * Clase para manejo de usuarios ['Alta, edicion, baja']
 * @author pablo
 *
 */


class users_RegistroController extends My_Controller_Action
{
	
	/**
	 * Extrae información del usuario de facebook
	 */
	private function getSessionDataUser(){
	
		$idUserFB =  $this->_facebook->getUser();
		if($idUserFB != 0 ) {
			$dataUser = $this->_facebook->api('/'.$idUserFB);
			return  $dataUser;
		} else {
			return false;
		}
	}
	

	/**
	 * Login para usuarios validación de credenciales
	 */
	public function loginAction(){

		
		$request	=	$this->getRequest();
		$error		=	$request->getParam("error",0);
		if($error){
			//			Zend_Debug::dump("el usuario no acepto los permisos");die;
			$this->_helper->flashMessenger->addMessage('Error | Usuario y/o contrase&ntilde;a incorrectos.');
		}
		
		
		$usuarioFacebook	=	$this->getSessionDataUser();
		$user    = new My_Model_Usuario();
		$login = $user->userlogin(@$usuarioFacebook["email"],$usuarioFacebook["id"]);
		if($login){
			$this->_redirect("/panel/");
		}

		
		$this->view->name	=	$usuarioFacebook["name"];
		$this->view->email	=	isset($usuarioFacebook["email"])?$usuarioFacebook["email"]:"";
	}
	
	
	/**
	 *  Guardar un usuario 
	 */	
	 public function guardarusuarioAction(){
		
		$request	=	$this->getRequest();
		$data		=	$request->getParams();
		$usuarioFacebook	=	$this->getSessionDataUser();
		$data["idFacebook"]		=	$usuarioFacebook["id"];
		
		$data["emailFacebook"]	=	isset($usuarioFacebook["email"])?$usuarioFacebook["email"]:$data["email"];
		
		if($data["email"] && $data["nombrecompleto"] && $data["terminos"] && $data["username"] && $data["idFacebook"]){
				$user    = new My_Model_Usuario();
				$login = $user->userlogin($data["emailFacebook"],$data["idFacebook"]);
					if($login){
						$this->_helper->json->sendJson(array("success"=>"ok"));
					} else {
						//si no esta lo doy de alta y lo logueo
						$usuario 	=	$user->saveUser($data);
						if($usuario){
							$login = $user->userlogin($data["emailFacebook"],$data["idFacebook"]);
							if($login)
							{	$this->_helper->json->sendJson(array("success"=>"ok"));}
							else{
								$this->_helper->flashMessenger->addMessage('Error | Usuario y/o contrase&ntilde;a incorrectos.');
								$this->_redirect("/");
							}
						}else{
							$this->_helper->flashMessenger->addMessage('Error | Usuario y/o contrase&ntilde;a incorrectos.');
							$this->_redirect("/");
						}
					}
		}
		else{
			$this->_redirect("/");
		}
		
		
	}
	
	/**
	 * Logout de usuarios
	 */
	public function logoutAction() {
		$auth = Zend_Auth::getInstance();
		$auth->clearIdentity();
		$this->_redirect('/');
	}
	
} 