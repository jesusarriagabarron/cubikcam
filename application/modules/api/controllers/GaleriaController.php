<?php

/**
 * Clase para administrar CRUD de Galerias
 * @author Jesus Arriaga
 *
 */

class api_galeriaController extends Zend_Rest_Controller {
	
	public $_auth;
	public $_em;
	private $_payload;
	
	public function headAction(){}
	
	public function init()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
		
		$this->getAuth();

		$request 		= 	$this->getRequest();
		$parametros 	=	$request->getParams();
		$contentType 	= 	$request->getHeader('Content-Type');
        $rawBody     	= 	$request->getRawBody();
        $this->_payload 	=	Zend_Json::decode($rawBody);
	}

	private function getAuth() {
		$auth = Zend_Auth::getInstance();
	
		if ($auth->getStorage()->read()) {
			$this->_auth = get_object_vars( $auth->getStorage()->read());
		} else {
			$this->_auth = false;
		}
	}
	
	/**
	*	Metodo CRUD Obtiene la consulta de todas las galerias
	*
	**/	
	public function indexAction() {
		$this->_helper->layout()->disableLayout(); 	
		
		$galerias = 	$this->_em->getRepository("Default_Model_Galeria")->findAll();

		$arrayGalerias = array();
		foreach($galerias as $galeria){
				$portada="";
				if(!$galeria->getImagenPortada()){
					$fotos = $galeria->getFotografias();
					if(!$fotos){$portada="noimage.jpg";}
					else{$portada = $fotos[0]->getFilename();}
				}


				$arrayGalerias[] = array(	
										 "id"			=>		$galeria->getId() ,
										 "nombre"		=>      $galeria->getNombreEvento(),
										 "fecha"		=>      $galeria->getFechaEvento()->getTimestamp()*1000,
										 "imagen" 		=>		$portada,
										 "createdAt"	=>		$galeria->getCreatedAt()->getTimestamp()*1000,
										 "updatedAt"	=>		$galeria->getUpdatedAt()->getTimestamp()*1000,
										 "status"		=>   	$galeria->getStatus()
 										);
		}

		$this->getResponse()->setHttpResponseCode(200);//Good
		$this->_helper->json->sendJson($arrayGalerias);
	}
	

	/**
	*	Metodo CRUD  get  que obtiene una galeria especifica dada un ID 
	*
	**/
	public function getAction()   {

		$idGaleria 	=	(int) $this->getRequest()->getParam("id");
		$galeria = 	$this->_em->find("Default_Model_Galeria",$idGaleria);

		if(!$galeria){
			$this->getResponse()->setHttpResponseCode(404);//error NOT FOUND
			$this->_helper->json->sendJson(array("response"=>$response));
		}

		$fotografias = $galeria->getFotografias();
		$arrayFotos  = array();

		foreach($fotografias as $foto){
			$arrayFotos[] = array("filename"=>$foto->getFilename());		
		}
		
		$response = array(
			                "id"			=>  $galeria->getId(),
							"nombre" 		=>	$galeria->getNombreEvento(),
							"status" 		=>	$galeria->getStatus(),
							"fechaEvento" 	=>	$galeria->getFechaEvento()->getTimestamp()*1000,
							"fotografias" 	=>  $arrayFotos
						);

		$this->getResponse()->setHttpResponseCode(200);//Good
		$this->_helper->json->sendJson($response);

	}


	/**
	*	Metodo CRUD   post   para insertar o crear una nueva galeria
	*
	**/
	
	public function postAction()  {

	


		$this->getResponse()->setHttpResponseCode(200);

		$nuevaGaleria 	=	new Default_Model_Galeria();
		$nuevaGaleria->setNombreEvento($this->_payload["nombre"]);
		$nuevaGaleria->setStatus(0);
		$fecha = new DateTime($this->_payload["fechaEvento"]);
		$nuevaGaleria->setFechaEvento($fecha);
		$nuevaGaleria->setCreatedAt();
		$nuevaGaleria->setUpdatedAt();

		$this->_em->persist($nuevaGaleria);
		$this->_em->flush();

		$idGaleria = $nuevaGaleria->getId();

		$response 	=	array( "response" => array( "idGaleria" => $idGaleria ) );

		$this->_helper->json->sendJson($response);
	}
	


	public function putAction() {
			$request 	=		$this->getRequest();
			$params 	=		$request->getParams();

			$idGaleria	 =		(int) $this->_payload["id"];
			$status      = 		(int) $this->_payload["status"];
			$nombre 	 =		filter_var($this->_payload["nombre"],FILTER_SANITIZE_STRING);
			$fechaEvento =		new DateTime();
			$fechaEvento->setTimestamp($this->_payload["fechaEvento"]/1000);


			$galeria 	 =		$this->_em->find("Default_Model_Galeria",$idGaleria);
			

			if(!$galeria){
				$this->getResponse()->setHttpResponseCode(404);
				$this->_helper->json->sendJson(array("response"=>"error no se pudo encontrar la galeria"));
			}

			$galeria->setStatus($status);
			$galeria->setFechaEvento($fechaEvento);
			$galeria->setNombreEvento($nombre);

			$this->_em->persist($galeria);
			$this->_em->flush();

			$this->_helper->json->sendJson(array("response"=>"success"));

	}
	
	public function deleteAction() {

	}

}