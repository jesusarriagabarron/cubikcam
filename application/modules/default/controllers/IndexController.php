<?php

/**
 * Modelo para controlar el Home y el login de los usuarios
 * @author jarriaga
 *
 */



class IndexController extends My_Controller_Action
{

	private $_payload;


	public function indexAction(){
		//$this->_helper->layout->disableLayout();
		
	}

	public function demoAction(){
		
	
	}

	public function serviciosAction(){


		
	}

	public function inboxAction(){
			$request 		= 	$this->getRequest();
			$parametros 	=	json_decode($request->getParam("mandrill_events"));

			$correo  = $parametros[0];

	     	$correosMandrill 	= 	new My_Model_CorreosMandrill();

	        		$usuario["email"]			=	$correo->msg->from_email;
	        		$usuario["nombreCompleto"]	=	'Correo Entrante Cubikcam';
	        		$usuario["tituloCorreo"]	=	$correo->msg->subject;
	        		$usuario["mensajeHtml"]		=	 $correo->msg->html;


	        		$correosMandrill->correoEntrante($usuario);

			$this->getResponse()->setHttpResponseCode(200);//error NOT FOUND
			$this->_helper->json->sendJson(array("success"=>"éxito"));
	}

	/**
	*	Metodo para galerias
	**/
	public function galeriasAction(){
		$galerias = $this->_em->getRepository("Default_Model_Galeria")->findBy(array('status'=>1),array('fechaEvento'=>'DESC'));
		$arrayGalerias = array();
		foreach($galerias as $galeria){
				$portada="";
				if(!$galeria->getImagenPortada()){
					$fotos = $galeria->getFotografias();
					if(!$fotos){$portada="noimage.jpg";}
					else{$portada = $fotos[0]->getFilename();}
				}else{
					$portada = $galeria->getImagenPortada();
				}

				$arrayGalerias[] = array(	
										 "id"			=>		$galeria->getId() ,
										 "nombre"		=>      $galeria->getNombreEvento(),
										 "fecha"		=>      $galeria->getFechaEvento()->getTimestamp()*1000,
										 "imagen" 		=>		$portada,
										 "createdAt"	=>		$galeria->getCreatedAt()->getTimestamp()*1000,
										 "updatedAt"	=>		$galeria->getUpdatedAt()->getTimestamp()*1000,
										 "status"		=>   	$galeria->getStatus()
 										);
		}
				$this->view->galerias   =  	$arrayGalerias; 
	}

	public function galeriaAction(){
		$this->view->doctype('XHTML1_RDFA');
		$request 	=	$this->getRequest();
		$idGaleria	=	(int) $request->getParam("id");
		$pagina      =	$request->getParam("page",1);


		$galeria 	=	$this->_em->find("Default_Model_Galeria",$idGaleria);

		if(!$galeria){$this->_redirect("/error/error9");}

		$fotografias = $galeria->getFotografias();
		$arrayFotos  = array();

		foreach($fotografias as $foto){
			$arrayFotos[] =array( 'filename' => $foto->getFilename() );
		}

		$paginator = Zend_Paginator::factory($arrayFotos );
	 	$paginator->setCurrentPageNumber($pagina);
	 	$paginator->setItemCountPerPage(12);
	 	$this->view->items=$paginator->getCurrentItemCount();
	 	$this->view->pagina=$paginator->getCurrentPageNumber();
	 	$this->view->paginador = $paginator;


		$this->view->fotografias = json_encode($arrayFotos);
		$this->view->galeria 	 = json_encode( array(	"nombre"=>$galeria->getNombreEvento(),
											"fechaEvento"=> $galeria->getFechaEvento()->getTimestamp()*1000,
											"id"=>$galeria->getId()
											));
		$this->view->galeriaInfo 	 =  array(	"nombre"=>$galeria->getNombreEvento(),
											"fechaEvento"=> $galeria->getFechaEvento()->getTimestamp()*1000,
											"id"=>$galeria->getId()
											);

	}





	public function rentaAction(){
		$request =  $this->getRequest();
		$p = $request->getParam("p","");

		if($p=="bronce" || $p == "plata" || $p=="oro"){
			$this->view->p = $p;
		}else{
			$this->view->p = "";
		}
	}


	public function peticionAction(){
		$request  = $this->getRequest();
		
		if($request->isPost()){
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			$request 		= 	$this->getRequest();
			$parametros 	=	$request->getParams();
			$contentType 	= 	$request->getHeader('Content-Type');
        	$rawBody     	= 	$request->getRawBody();
        	$this->_payload 	=	Zend_Json::decode($rawBody);

        	if($this->_payload){	

        		$nombreCompleto =   filter_var($this->_payload["nombreCompleto"],FILTER_SANITIZE_STRING) ;
        		$email			=	filter_var($this->_payload["email"],FILTER_SANITIZE_EMAIL);
        		$telefono 		=	filter_var($this->_payload["telefono"],FILTER_SANITIZE_STRING);
        		$ciudadEvento	=	filter_var($this->_payload["ciudad"],FILTER_SANITIZE_STRING);
        		$estadoEvento 	=	filter_var($this->_payload["estado"],FILTER_SANITIZE_STRING);
        		$paquete 		=	filter_var($this->_payload["paquete"],FILTER_SANITIZE_STRING);
        		$fechaEvento =		new DateTime();
				$fechaEvento->setTimestamp($this->_payload["fecha"]/1000);
        		$comentarios 	=	filter_var($this->_payload["comentarios"],FILTER_SANITIZE_STRING);



        		try{

	        		//Salvamos la petición de datos

	        		$solicitud 		=	new Default_Model_Solicitud();
	        		$solicitud->setNombreCompleto($nombreCompleto);
	        		$solicitud->setEmail($email);
	        		$solicitud->setTelefono($telefono);
	        		$solicitud->setCiudadEvento($ciudadEvento);
	        		$solicitud->setEstadoEvento($estadoEvento);
	        		$solicitud->setPaquete($paquete);
	        		$solicitud->setFechaEvento($fechaEvento);
	        		$solicitud->setComentarios($comentarios);
	        		$solicitud->setCreatedAt();


	        		$this->_em->persist($solicitud);
	        		$this->_em->flush();

	        		//enviamos el correo electrónico 
	        		$correosMandrill 	= 	new My_Model_CorreosMandrill();

	        		$fecha = $fechaEvento->format( 'd-m-Y' );

	        		$usuario["email"]			=	$email;
	        		$usuario["nombreCompleto"]	=	$nombreCompleto;
	        		$usuario["tituloCorreo"]	=	"Solicitud de informes";
	        		$usuario["mensajeHtml"]		=	"<p style='text-align:justify;'>
	        											Recibimos tu solicitud de información sobre la renta de 
	        											nuestra cabina fotográfica (Photobooth), en breve nos 
	        											pondremos en contacto contigo para darte los detalles acerca del 
	        											servicios, estamos agradecidos en tu interés en nosotros, Gracias.
	        											<br><br>
	        											Detalle de solicitud:
	        											<br><br>
	        											<strong>Nombre</strong>: {$nombreCompleto}<br>
	        											<strong>Email de contacto</strong>: {$email}<br>
	        											<strong>Paquete solicitado</strong>: {$paquete}<br>
	        											<strong>Ciudad del evento</strong>: {$ciudadEvento}<br>
	        											<strong>Estado del evento</strong>: {$estadoEvento}<br>
	        											<strong>Fecha </strong>: {$fecha}<br>
	        											<strong>Tus comentarios</strong>: {$comentarios}<br>
	        											<br>
	        										</p>";

	        		$correosMandrill->correoGeneral($usuario);
	        		
	        		$usuario["email"]			=		"jarriagabarron@gmail.com";
	        		$usuario["tituloCorreo"] 	=		"Posible cliente: ".$nombreCompleto;

	        		$correosMandrill->correoGeneral($usuario);

	        		//retornamos éxito!
	        		$this->_helper->json->sendJson(array("response"=>"success"));


        		}catch( Exception $e){

        			$this->getResponse()->setHttpResponseCode(404);//error NOT FOUND
					$this->_helper->json->sendJson(array("error"=>"la página no existe"));
        		}

        		

        	}
		}

		$this->getResponse()->setHttpResponseCode(404);//error NOT FOUND
		$this->_helper->json->sendJson(array("error"=>"la página no existe"));
	}
	
 
} 