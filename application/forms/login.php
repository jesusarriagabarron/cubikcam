<?php

/**
 * Formulario para registro externo
 * @author Pablo
 *
 */

class Application_Form_login extends Zend_Form{
	public function init(){
		$this->setMethod('post');
		
		$usuario = new Zend_Form_Element_Text('usuario');
		$usuario->setAttrib('placeholder','Usuario')
			->removeDecorator('Label')
			->setOptions (array('class'=>'form-control', 'type'=>'text'))
			->setRequired(true)
			->addFilters(array('StringTrim'));
		
		
		$password = new Zend_Form_Element_Password('password');
		$password->setLabel('')->setAttrib('placeholder','Contraseña')
			->removeDecorator('Label')
			->setOptions (array('class'=>'form-control'))
			->setRequired(true);
		
		
		$this->addDisplayGroup(array(
				$usuario
		),'usuario',array());
		
		$this->addDisplayGroup(array(
				$password
		),'password',array());

		
	}
}