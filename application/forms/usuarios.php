<?php

/**
 * Formulario para registro externo
 * @author Pablo
 *
 */

class Application_Form_usuarios extends Zend_Form{
	
	public function init(){
		
		$this->setMethod('post');
		$this->setAttrib('rol','form');
		
		$nombre = new Zend_Form_Element_Text('nombre');
		$nombre->setLabel('Nombre:')->setAttrib('placeholder','Nombre')
			->setOptions (array('class'=>'form-control'))
			->setRequired(true)
			->addErrorMessages(array())
			->addFilters(array('StringTrim', 'StripTags'));
		
		
		
		$apaterno = new Zend_Form_Element_Text('apaterno');
		$apaterno->setLabel('Apellido Paterno')->setAttrib('placeholder','Apellido paterno')
			->setOptions(array('class'=>'form-control'))
			->setRequired(true)
			->addFilters(array('StringTrim', 'StripTags'));
		
		
		
		$amaterno = new Zend_Form_Element_Text('amaterno');
		$amaterno->setLabel('Apellido Materno')->setAttrib('placeholder','Apellido materno')
			->setOptions(array('class'=>'form-control'))
			->addFilters(array('StringTrim', 'StripTags'));
		
		
		// Datos principales de la cuenta
		$email = new Zend_Form_Element_Text('e-amil');
		$email->setLabel('Correo electrónico')->setAttrib('placeholder','Correo electrónico')
			->setOptions (array('class'=>'form-control'))
			->setRequired(true)
			->addErrorMessages(array())
			->addFilters(array('StringTrim', 'StripTags'))
			->addValidator('EmailAddress')
			->addValidator(new Zend_Validate_Db_NoRecordExists(array(
                                                                'field'=>'email',
                                                                'table'=>'Usuario'
                                                               )));
			
		
		
		$required = new Zend_Validate_NotEmpty ();
		$required->setType ($required->getType() | Zend_Validate_NotEmpty::INTEGER | Zend_Validate_NotEmpty::ZERO);
		
		$estado = new Zend_Form_Element_Select('Estado');
		$estado->setLabel('Estado')
		->setValidators(array ($required))
		->setRequired(true)
		->setOptions (array('class'=>'form-control'))
			->addMultiOptions(array(
						'0'  => 'ESTADO',
						'1'  => 'Aguas Calientes',				
						'2'  => 'BajaCalifornia',
						'3'  => 'BajaCalifornia Sur',
						'4'  => 'Campeche',
						'5'  => 'Coahuila de Zaragoza',
						'6'  => 'Colima',
						'7'  => 'Chiapas',
						'8'	 => 'Chihuahua',
						'9'  => 'Distrito Federal',
						'10' => 'Durango',
						'11' => 'Guanajuato',
						'12' => 'Guerrero',
						'13' => 'Hidalgo',
						'14' => 'Jalisco',
						'15' => 'México',
						'16' => 'Michoacán de Ocampo',
						'17' => 'Morelos',
						'18' => 'Nayarit',
						'19' => 'Nuevo León',
						'20' => 'Oaxaca',
						'21' => 'Puebla',
						'22' => 'Querétaro',
						'23' => 'Quintana Roo',
						'24' => 'San Luis Potosí',
						'25' => 'Sinaloa',
						'26' => 'Sonora',
						'27' => 'Tabasco',
						'28' => 'Tamaulipas',
						'29' => 'Tlaxcala',
						'30' => 'Veracruz',
						'31' => 'Yucatán',
						'32' => 'Zacatecas'
					));
		
		
		$direccion = new Zend_Form_Element_Text('direccion');
		$direccion->setLabel('Dirección')->setAttrib('placeholder','Direccion')
			->setOptions(array('class'=>'form-control'))
			->setRequired(true)
			->addFilters(array('StringTrim', 'StripTags'));
		
			
		
		$telefono = new Zend_Form_Element_Text('telefono');
		$telefono->setLabel('Teléfono')->setAttrib('placeholder','Teléfono')
			->setOptions(array('class'=>'form-control'))
			->setRequired(true)
			->addFilters(array('StringTrim', 'StripTags'));
		
		
		$username = new Zend_Form_Element_Text('user-name');
		$username->setLabel('Usuario')->setAttrib('placeholder','Nombre de usuario')
			->setOptions(array('class'=>'form-control'))
			->setRequired(true)->addErrorMessages(array())
			->addFilters(array('StringTrim', 'StripTags'))
			->addValidator(new Zend_Validate_Alnum())
			->addValidator(new Zend_Validate_Db_NoRecordExists(array(
															'field'=>'nombre',
															'table'=>'Usuario'
													)));
		
		$passwd = new Zend_Form_Element_Password('password');
		$passwd->setLabel('Contraseña')->setAttrib('placeholder','Contraseña')
			->setOptions(array('class'=>'form-control'))
			->setRequired(true)->addErrorMessage('Escribe tu contraseña')
			->addFilters(array('StringTrim', 'StripTags', 'HtmlEntities'));
		
		
		$passwdconf = new Zend_Form_Element_Password('password2');
		$passwdconf->setLabel('Confirma tu contraseña')->setAttrib('placeholder','Confirma tu contraseña')
			->setOptions(array('class'=>'form-control'))
			->setRequired(true)->addErrorMessage('Confirma tu contraseña')
			->addFilter('HtmlEntities')
			->addValidator('Identical',false,array('token'=>'password'));
		
		
		$submit = new Zend_Form_Element_Button('submit');
		$submit->setlabel('Enviar')
			->setOptions(array('class'=>'btn btn-clean-dark btn-lg'))
			->setAttrib('id',   'saveregistro')
			->setAttrib('type', 'submit');
		
		
		
		$this->addElements(array(
				$nombre,
				$apaterno,
				$amaterno,
				$email,
				$estado,
				$direccion,
				$telefono,
				$username,
				$passwd,
				$passwdconf,
				$submit));
		
		$this->addDisplayGroup(array(
				$nombre,
				$apaterno,
				$amaterno,
				$email,
				$estado,
				$direccion,
				$telefono
				),'contact',array());
		
		$this->addDisplayGroup(array(
				$username,
				$passwd,
				$passwdconf,
				$submit
		),'pass',array());
	}
}