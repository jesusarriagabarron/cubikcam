<?php


/** @Entity 
 * @Table(name="Galeria")
 * */
class Default_Model_Galeria {
	
    /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;

    /** @Column(type="string",length=150) **/
    private $nombreEvento;

    /** @Column(type="smallint") **/
    private $status;

	/** @Column(type="datetime") **/
	private $fechaEvento;

   	/** @Column(type="datetime") **/
	private $createdAt;
	
	/** @Column(type="datetime") **/
	private $updatedAt;

	/** @Column(type="string",length=100,nullable=true) **/
	private $imagenPortada;


	 /**
	 * @OneToMany(targetEntity="Default_Model_Foto",mappedBy="galeria")
	 **/
	private $fotografias;


	/** Seters **/

	public function setNombreEvento($nombre){		$this->nombreEvento		=	$nombre;	}
	public function setStatus($status){				$this->status 			=	$status;	}
	public function setFechaEvento($fecha){			$this->fechaEvento		=	$fecha;		}
	public function setCreatedAt(){					$this->createdAt = new DateTime("now");	}
	public function setUpdatedAt(){					$this->updatedAt = new DateTime("now");	}
	public function setImagePortada($imagen){		$this->imagenPortada = $imagen;			}
	public function setFotografia(Default_Model_Foto $fotografia){		$this->fotografias[] = $fotografia;		}

	/** Getters */

	public function getId(){					return 		$this->id;						}

	public function getNombreEvento(){			return 		$this->nombreEvento;			}
	public function getStatus(){				return 		$this->status;					}
	public function getFechaEvento(){			return 		$this->fechaEvento;				}
	public function getCreatedAt(){				return 		$this->createdAt;				}
	public function getUpdatedAt(){				return 		$this->updatedAt;				}
	public function getImagenPortada(){			return 		$this->imagenPortada;			}
	public function getFotografias(){			return 		$this->fotografias;				}

}