<?php


/** @Entity 
 * @Table(name="Foto")
 * */
class Default_Model_Foto {
	
    /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;

    /** @Column(type="string",length=150) **/
    private $filename;

   /**
	 * @ManyToOne(targetEntity="Default_Model_Galeria",inversedBy="fotografias")
	 * @JoinColumn(name="idGaleria",referencedColumnName="id")
	 */
	private $galeria;


	public function setFilename($nombreFoto){			$this->filename 	=	$nombreFoto;		}
	public function setGaleria(Default_Model_Galeria $galeria){		$this->galeria = $galeria;	 	}


	public function getId(){			return 		$this->id;			}
	public function getFilename(){		return 		$this->filename;	}
	public function getGaleria(){		return 		$this->galeria;		}
	
}