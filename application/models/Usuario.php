<?php


/** @Entity 
 * @Table(name="Usuario")
 * */
class Default_Model_Usuario {
	
    /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;

    /** @Column(type="string",  length=150) **/
    private $username;
    
    /** @Column(type="string",  length=150) **/
    private $nombre;
    
	/** @Column(type="string",unique=true,length=100) **/
	private $email;
	
	/** @Column(type="string",nullable=true,length=200) **/
	private $password;
	
	/** @Column(type="datetime") **/
	private $createdAt;
	
	/** @Column(type="datetime") **/
	private $updatedAt;
			
	
	/** @Column(type="smallint")  **/
	private $status;
	
	/** @Column(type="smallint")  **/
	private $rol;
	
	
	/** Seters **/
	public function setNombre($nombre)          {  $this->nombre =  $nombre; }
	public function setUsername($username)          {  $this->username =  $username; }
	public function setEmail($email)            {  $this->email =  $email;   }

	public function setPassword($password)      {  $this->password  =  $password; }
	public function setRol($rol)                {  $this->rol = $rol; }
	public function setStatus($status)        {  $this->status = $status;  }

	public function setCreatedAt(){$this->createdAt = new DateTime("now");}
	public function setUpdatedAt($update){$this->updatedAt = $update;}

	
	
	
	/** Getters **/
	public function getId()          { return $this->id; }
	public function getNombre()      { return $this->nombre; }
	public function getEmail()       { return $this->email; }

	public function getPassword()    { return $this->password; }
	public function getRol()         { return $this->rol; }
	public function getStatus()    { return $this->status; }
	public function getUsername()	{return $this->username;}
	
	public function getCreatedAt()	{	return $this->createdAt;}
	public function getUpdatedAt()	{	return $this->updatedAt;}

	
}
