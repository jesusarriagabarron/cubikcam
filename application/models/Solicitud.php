<?php


/** @Entity 
 * @Table(name="Solicitud")
 * */
class Default_Model_Solicitud {
	
    /**
    * @Id
    * @GeneratedValue(strategy="AUTO")
    * @Column(type="integer")
    */
    private $id;

    /** @Column(type="string",length=150,nullable=true) **/
    private $nombreCompleto;

    /** @Column(type="string",length=150,nullable=true) **/
    private $email;

    /** @Column(type="string",length=50,nullable=true) **/
    private $telefono;

    /** @Column(type="string",length=100,nullable=true) **/
    private $ciudadEvento;

    /** @Column(type="string",length=50,nullable=true) **/
    private $estadoEvento;

    /** @Column(type="string",length=10,nullable=true) **/
    private $paquete;

    /** @Column(type="datetime",length=50,nullable=true) **/
    private $fechaEvento;

    /** @Column(type="datetime",length=50,nullable=true) **/
    private $createdAt;

    /** @Column(type="text",nullable=true) **/
    private $comentarios;



	public function setNombreCompleto($nombreCompleto){			$this->nombreCompleto 	=	$nombreCompleto;		}
	public function setEmail($email){							$this->email 			=	$email;					}
	public function setTelefono($telefono){						$this->telefono 		=	$telefono;				}
	public function setCiudadEvento($ciudadEvento){				$this->ciudadEvento 	=	$ciudadEvento;			}
	public function setEstadoEvento($estadoEvento){  			$this->estadoEvento   	= 	$estadoEvento;			}
	public function setPaquete($paquete){						$this->paquete 			=	$paquete;				}
	public function setFechaEvento($fechaEvento){  				$this->fechaEvento 		=	$fechaEvento;			}
	public function setCreatedAt(){								$this->createdAt 		=	new DateTime("now");	}
	public function setComentarios($comentarios){				$this->comentarios 		=	$comentarios;			}

	


	public function getId(){			return 		$this->id;						}
	public function getNombreCompleto(){ 		return $this->nombreCompleto;		}
	public function getEmail(){					return $this->email;				}
	public function getTelefono(){				return $this->telefono;				}
	public function getCiudadEvento(){			return $this->ciudadEvento;			}
	public function getEstadoEvento(){			return $this->estadoEvento;			}
	public function getPaquete(){				return $this->paquete;				}
	public function getFechaEvento(){			return $this->fechaEvento;			}
	public function getCreatedAt(){				return $this->createdAt;			}
	public function getComentarios(){			return $this->comentarios;			}
	
}