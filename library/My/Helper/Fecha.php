<?php
/**
 * Devuelve la fecha en formato
 * @author jarriaga
 *
 */

class My_Helper_Fecha extends Zend_View_Helper_Abstract{

	public function fecha($fecha,$pais="es_MX",$tipo = "full"){
		if($fecha != null) {
			$fecha = new Zend_Date($fecha->format('Y-m-d H:i:s'));
			if($tipo!="full"){
				switch ($tipo){
					case "short":
							$fecha = $fecha->get(Zend_Date::DATE_MEDIUM,$pais);
							break;	
					case "medium":
							$fecha = strtoupper($fecha->get(Zend_Date::MONTH_NAME_SHORT,$pais)." ".$fecha->get(Zend_Date::DAY,$pais).", ".$fecha->get(Zend_Date::YEAR_SHORT,$pais));
							break;
					case "fullhoras":
							$fecha = $fecha->get(Zend_Date::DATE_FULL,$pais).", a las ".$fecha->get(Zend_Date::TIME_SHORT). " horas";
							break;
					case "iso":
							$fecha	=	$fecha->getIso();
							break;
				}
			}
			else{
				$fecha = $fecha->get(Zend_Date::DATE_FULL,$pais);
			}
			return $fecha;
		} else {
			return false;
		}
			
	}
}