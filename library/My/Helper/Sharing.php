<?php
/**
 * Devuelve la fecha en formato
 * @author plopez
 *
 */

class My_Helper_Sharing extends Zend_View_Helper_Abstract{

	public function Sharing(){
		
		$url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		
		
		
		$facebook      = '<div id="shfb" style="position: absolute;"><div class="fb-share-button" data-href="'.$url.'" data-type="button_count"></div></div>';
		$twitterlink   = '<div id="shtw" style="position: absolute; left: 176px; "><a href="'.$url.'" class="twitter-share-button">Tweet</a> </div>';
		$twitterscript = "<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>";
		$googlescript  = '<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>';
		$google        = '<div id="shgg" style="position: absolute;  margin-left: 207px;"><g:plusone size="tall"></g:plusone></div>';
		 
		$sharing =  $twitterlink.' '.$twitterscript.$facebook.$googlescript.$google;
		
		return $sharing;
	}
	
}