<?php
/**
 * Devuelve el numero de notificaciones nuevas
 * @author jarriaga
 *
 */

class My_Helper_TienesPub extends Zend_View_Helper_Abstract{

	private $_auth;
	private $_em;
	
	public function init(){
		$auth = Zend_Auth::getInstance();
		if ($auth->getStorage()->read()) {
			$this->_auth = get_object_vars( $auth->getStorage()->read());
		} else {
			$this->_auth = false;
		}
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
	}
	
	public function tienesPub(){
			$this->init();
				$usuario  = $this->_em->find("Default_Model_Usuario",$this->_auth["id"]);
				$espacios = $usuario->getEspaciosTotales();
				if($espacios>0)
					return true;
				else 
					return false;
	}
}