<?php
/**
 * Devuelve una cantidad en forma numerica local
 * @author jarriaga
 *
 */

class My_Helper_Numero extends Zend_View_Helper_Abstract{

	
	public function numero($monto,$pais='es_MX'){
		$numero = Zend_Locale_Format::toNumber((int)$monto,array('locale'=>$pais));
		return $numero; 		
	}
}