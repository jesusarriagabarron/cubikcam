<?php
/**
 * Creación de ACL para permisos 
 * @author jarriaga
 *
 */
class My_Acl extends Zend_Acl{
	
	public function __construct(){
		//creamos los roles 
		$this->addRole(new Zend_Acl_Role('guest'));
		$this->addRole(new Zend_Acl_Role('user'),'guest');
		$this->addRole(new Zend_Acl_Role('admin'),'user');
		
		
		$this->add(new Zend_Acl_Resource('default-index'));
		$this->add(new Zend_Acl_Resource('default-error'));
		$this->add(new Zend_Acl_Resource('users-registro'));
		$this->add(new Zend_Acl_Resource('panel-index'));
		$this->add(new Zend_Acl_Resource('panel-apianzuelo'));
		$this->add(new Zend_Acl_Resource('panel-apicharlas'));

		$this->add(new Zend_Acl_Resource('admin-index'));
		$this->add(new Zend_Acl_Resource('admin-panel'));
		$this->add(new Zend_Acl_Resource('api-galeria'));


		
		//Creamos los permisos
		//permisos invitado
		$this->allow('guest','default-index');
		$this->allow('guest','default-error');
		$this->allow('guest','users-registro');

		$this->allow('guest','admin-index');
		
		
		//permisos de usuario

		$this->allow('user','panel-index');
		$this->allow('user','panel-apianzuelo');
		$this->allow('user','panel-apicharlas');
		$this->allow('user','admin-panel');
		$this->allow('user','api-galeria');

		
	}
	
}