<?php 

/**
 * Definición de clase de controlador genérico
 *
 * @package library.My.Controller
 * @author pil
 */


require_once 'facebookSDK/facebook.php';
require_once 'twitteroauth/twitteroauth.php';

class My_Controller_Action extends Zend_Controller_Action 
{
	/**
	 * Contextos, se pueden modificar en cada uno de los controladores hijos
	 *
	 * @var $contexts: arreglo de contextos
	 */
	public $contexts = array( );
	public $_auth;
	public $_em;
	public $_appid;
	public $_secret;
	public $_facebook;
	public $_twitter;
	public $_tweets ;
	
	public function init() {
		$this->view->doctype('XHTML1_RDFA');
		date_default_timezone_set('America/Mexico_City');
		
		if($this->_request->isXmlHttpRequest())
			$this->_request->setParam("format","json");
	
		$contextSwitch = $this->_helper->contextSwitch();
		$contextSwitch->initContext();
		
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
		
		$this->getAuth();
		
		
		$config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
		$fbkeys = $config->getOption('facebook');
		$this->_appid  = $fbkeys['appid'];
		$this->_secret = $fbkeys['appsecret'];
		
		$this->_facebook = new Facebookphp(array(
				'appId'  => $this->_appid,
				'secret' => $this->_secret
		));

		$this->_twitter = new TwitterOAuth('h6KU5cYpjJDqrSjwjMifmerHn','ZMuiya5LJ4APIx3s5FAbYrXv2F9XQM8Z6bW9RdlplIWdMUCSjm',
		 									'2818854170-tpkWhmkYTpHgFKFKtaPOZkqGjM94OQBZ3r0ZTXE', 'SAPPaSbLPANrlvlOJrAP3maCFtMPTO6q5nV5L9FQAFf71');

		$this->_twitter->ssl_verifypeer = true;

		$this->_tweets = $this->_twitter->get('statuses/user_timeline', array('screen_name' => 'cubikcam', 'exclude_replies' => 'true', 'include_rts' => 'false', 'count' => 3));
		$this->view->tweets = $this->_tweets;

	
		$protocol = isset($_SERVER['HTTPS']) ? 'https' : 'http';
		$server = $_SERVER['HTTP_HOST'];
		
		
		$this->view->urlLoginFb = $this->_facebook->getLoginUrl(array('redirect_uri' => $protocol."://".$server.$fbkeys['redirect_uri'],
								'scope'=>'email,user_friends,user_about_me'));
		$idUserFB =  $this->_facebook->getUser();
		
		//cambiamos el template si se trata de una unidad medica
		$moduleName		=	$this->getRequest()->getModuleName();
		$controllerName		=	$this->getRequest()->getControllerName();
		if($moduleName	==	"admin" && $controllerName == "panel")
			$this->_helper->_layout->setLayout('adminlayout');
	}
	

	private function getAuth() {
		$auth = Zend_Auth::getInstance();
		
		if ($auth->getStorage()->read()) {
			$this->_auth = get_object_vars( $auth->getStorage()->read());
		} else {
			$this->_auth = false;
		}
	}
	
}