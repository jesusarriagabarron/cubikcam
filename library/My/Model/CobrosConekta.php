<?php

//cargamos el sdk de Conekta
require_once("conektaSDK/Conekta.php");


class My_Model_CobrosConekta	{
	
	private $_em;
	private $_auth;
	
	public function __construct(){
		$registro = Zend_Registry::getInstance();
		$this->_em	=	$registro->entitymanager;
		$auth = Zend_Auth::getInstance();
		if($auth->getStorage()->read())
			$this->_auth = get_object_vars( $auth->getStorage()->read());
	}
	
	/**
	 * asignamos el tipo de cobro y ejecutamos su funcion
	 */
	public function procesarCobro($tipoCobro, $params){
		Conekta::setApiKey("key_1Zd4q1qWPdUmMP1Z");
		$response="";
		switch ($tipoCobro){
			case 'tarjeta'	:
					$response = $this->tarjeta($params);			
					break;
		}
		//si hay un error
		if($response->object=="error"){
			$respuesta["error"]		=	1;
			$respuesta["mensaje"]	=	"El pago no puede ser procesado";
			$respuesta["odc"]		=	$params["odc"];
		}else{
		//si no hay error entonces salvamos el movimiento
			$respuesta["error"]		=	0;
			$respuesta["response"]	= 	$response;
			$respuesta["odc"]		=	$params["odc"];
		}
		//Zend_Debug::dump($respuesta);die;
		return $respuesta;
	}
	
	
	/**
	 * Cobro para tarjetas de crédito
	 * @param unknown $params
	 * @return unknown
	 */
	public function tarjeta($params){
		$cargo	=	Conekta_Charge::create(
					array(
								"description"	=>	$params["description"],
								"amount"		=>	$params["amount"],
								"currency"		=>	$params["currency"],
								"reference_id"	=>	strtoupper(uniqid($params["id"]."I")),//generamos el uniqid (  ID[numeroid]
								"card"			=>	$params["conektaTokenId"]						
						)
					);//Fin conekta_charge
		
		if($cargo->object=="charge" ){
			// Si el cargo  y su status es pagado, entonces creamos el cargo
			// y cambiamos el status de la orden de compra
			$usuario =	$this->_em->find('Default_Model_Usuario',$this->_auth['id']);
			$nuevoCargo		=		new Default_Model_Cargo();
			$nuevoCargo->setChargeId		(		$cargo->id							);
			$nuevoCargo->setStatus			(		$cargo->status						);
			$nuevoCargo->setCurrency		(		$cargo->currency					);
			$nuevoCargo->setDescription		(   	$cargo->description					);
			$nuevoCargo->setReference		(		$cargo->reference_id				);
			$nuevoCargo->setPaymentMethod	( 		$cargo->payment_method->object		);
			$nuevoCargo->setNameCard		(		$cargo->payment_method->name		);
			$nuevoCargo->setLast4			(		$cargo->payment_method->last4		);
			$nuevoCargo->setBrand			( 		$cargo->payment_method->brand		);
			$nuevoCargo->setAmount			(		$cargo->amount						);
			$nuevoCargo->setComision		(		$cargo->fee							); 				
			$nuevoCargo->setFechaHoraTS		(		$cargo->created_at					);
			$nuevoCargo->setOdc				(		$params["odc"]						);
			$nuevoCargo->setUsuario			(		$usuario							);
			
			$this->_em->persist($nuevoCargo);
			$this->_em->flush();
			// si el cobro fue  PAGADO con éxito entonces 
			if($cargo->status=="paid"){
				$ODC	=	$this->_em->find('Default_Model_OrdenDeCompra',$params["odc"]);
				//Asignamos el cargo
				$ODC->setChargeId($cargo->id,$this->_em);
				//Cambiamos el status a pagado
				$ODC->setStatus_EnPagoConfirmado($this->_em);
				$correo =	new My_Model_CorreosMandrill();
				$correo->sendVoucherPago($ODC);
				$correo->sendNuevaOrden($ODC);
			}else{
				$ODC	=	$this->_em->find('Default_Model_OrdenDeCompra',$params["odc"]);
				$ODC->setChargeId($cargo->id,$this->_em);
			}
		}
		
		return $cargo;
	}
	
	
}