<?php

/**
 * Manejo de envío de correos 
 */
require_once("mandrillSDK/Mandrill.php");

class My_Model_CorreosMandrill	{

	private $_em;
	private $_auth;
	private $_mandrill;
	private $_twitter;
	private $_facebook;
	private $_config;
	private $_host="";

	public function __construct(){
		
		$registro = Zend_Registry::getInstance();
		$this->_em	=	$registro->entitymanager;
		
		//iniciamos mandrill
		$this->_mandrill = new Mandrill("7a_vHPi_REGrJ4st8aGe7w");
		$this->_facebook	=	"https://www.facebook.com/Cubikcam";
		$this->_twitter		=	"https://www.twitter.com/Cubikcam";
		
		$request = Zend_Controller_Front::getInstance()->getRequest();
		$url = $request->getScheme() . '://' . $request->getHttpHost();
		$this->_host=$url;
		
	}
	
	/**
	 * Crea un correo con la plantilla GENERAL ... solo cambia los parámetros y crea tu propio mensaje...
	 * 
	 * @param Default_Model_Usuario $usuario
	 */
	public function correoGeneral($usuario){
		//Datos generales
		$this->_config["emailDestino"]  = 	$usuario["email"];
		$this->_config["nombreDestino"] =	$usuario["nombreCompleto"];
		$this->_config["titulo"]		=	$usuario["tituloCorreo"];
		//identificador para mandril ¡Muy importante para catalogar correos!
		$this->_config["identificador"]	=   "cubikcam";
		//Mensaje en HTML
		$this->_config["mensajeHTML"]=$usuario["mensajeHtml"];
		$this->sendTemplate();
	}


	public function correoEntrante($usuario){
		//Datos generales
		$this->_config["emailDestino"]  = 	$usuario["email"];
		$this->_config["nombreDestino"] =	$usuario["nombreCompleto"];
		$this->_config["titulo"]		=	$usuario["tituloCorreo"];
		//identificador para mandril ¡Muy importante para catalogar correos!
		$this->_config["identificador"]	=   "cubikcam";
		//Mensaje en HTML
		$this->_config["mensajeHTML"]=$usuario["mensajeHtml"];
		$this->sendEntrante();
	}

	
	public function sendTemplate(){
		$f=new DateTime('now');
		
		$template_name= "cubikcam";
		$template_content = array(
				array(
						"name" => "",
						"content" => ""
				)
		);
		$message = array(
				"subject"		=>	$this->_config["titulo"],
				"from_email"	=>	"contacto@cubikcam.com",
				"from_name"		=>	"Cubikcam Photobooth",
				"to"			=>	array(
										array(
										"email"		=>		$this->_config["emailDestino"] ,
										"name"		=>		$this->_config["nombreDestino"] ,
										"type"		=>		"to"
									 	)
	        					  	),
				"important"		=>	false,
				"track_opens" => null,
				"track_clicks" => null,
				"auto_text" => null,
				"auto_html" => null,
				"inline_css" => null,
				"url_strip_qs" => null,
				"preserve_recipients" => null,
				"view_content_link" => null,
				"tracking_domain" => null,
				"signing_domain" => null,
				"return_path_domain" => null,
				"merge" => true,
				"tags" => array($this->_config["identificador"]),
				"global_merge_vars" => array(
						array("LIST:COMPANY"				=> "Cubikcam")
				),
				"merge_vars" => array(
						array(
								"rcpt" => $this->_config["emailDestino"] ,
								"vars" => array(
										array(
												"name" => "COMPRADOR",
												"content" => $this->_config["nombreDestino"] 
										),
										array(
												"name" => "COMPANY",
												"content" => "Cubikcam"
										),
										array(
												"name" => "CURRENT_YEAR",
												"content" => $f->format('Y')  
										),
										array(
												"name"		=>	"MENSAJE",
												"content"	=>	$this->_config["mensajeHTML"]
										),
										array(	"name"	=>	"TWITTER","content"	=>	$this->_twitter	),
										array(	"name"	=>	"FACEBOOK","content"	=>	$this->_facebook )
								)
						)
				),
				"recipient_metadata" => array(
						array(
								"rcpt" => $this->_config["emailDestino"],
						)
				),
		);// Fin array message
				
		$async = false;
		$ip_pool = "Main Pool";
		$send_at = date("Y-m-d hh:ii:ss",strtotime("-1 day"));
		try{
			$result = $this->_mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool);
		} catch(Mandrill_Error $e) {
		
		}
	}



	public function sendEntrante(){
		$f=new DateTime('now');
		
	
		$message = array(
				'html' => $this->_config["mensajeHTML"],
				"subject"		=>	$this->_config["titulo"],
				"from_email"	=>	$this->_config["emailDestino"],
				"from_name"		=>	$this->_config["nombreDestino"],
				"to"			=>	array(
										array(
										"email"		=>		'jarriagabarron@gmail.com',
										"name"		=>	'Jesus Arriaga' ,
										"type"		=>		"to"
									 	)
	        					  	),
				"important"		=>	false,
				"track_opens" => null,
				"track_clicks" => null,
				"auto_text" => null,
				"auto_html" => null,
				"inline_css" => null,
				"url_strip_qs" => null,
				"preserve_recipients" => null,
				"view_content_link" => null,
				"tracking_domain" => null,
				"signing_domain" => null,
				"return_path_domain" => null,
				"merge" => true,
				"tags" => array('correo-entrante'),
				"global_merge_vars" => array(
						array("LIST:COMPANY"				=> "Cubikcam")
				),
				"merge_vars" => array(
						array(
								"rcpt" => $this->_config["emailDestino"] ,
								"vars" => array(
										array(
												"name" => "COMPRADOR",
												"content" => $this->_config["nombreDestino"] 
										),
										array(
												"name" => "COMPANY",
												"content" => "Cubikcam"
										),
										array(
												"name" => "CURRENT_YEAR",
												"content" => $f->format('Y')  
										),
										array(
												"name"		=>	"MENSAJE",
												"content"	=>	$this->_config["mensajeHTML"]
										),
										array(	"name"	=>	"TWITTER","content"	=>	$this->_twitter	),
										array(	"name"	=>	"FACEBOOK","content"	=>	$this->_facebook )
								)
						)
				),
				"recipient_metadata" => array(
						array(
								"rcpt" => 'jarriagabarron@gmail.com',
						)
				),
		);// Fin array message
				
		$async = false;
		$ip_pool = "Main Pool";
		$send_at = date("Y-m-d h:i:s",strtotime("-1 day"));
		try{
			$result = $this->_mandrill->messages->send($message, $async, $ip_pool);
		} catch(Mandrill_Error $e) {
		
		}
	}


}