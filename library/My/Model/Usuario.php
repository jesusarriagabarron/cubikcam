<?php
class My_Model_Usuario extends Zend_Db_Table_Abstract{
	
	private $_em;
	
	
	public function init() {
		$registry = Zend_Registry::getInstance();
		$this->_em = $registry->entitymanager;
	}
	
	
	/**
	 * Guardamos los datos de la cuenta de usuario
	 * @param array $data
	 */
	public function saveUser($data){
		
		$validador = new My_Validador();
		$username 				= trim(addslashes($data['username']));
		$nombre					= trim(addslashes($data['nombrecompleto']));
		$emailNotificaciones   	= 	$validador->mailValido($data['email']);
		$emailFacebook    		= 	$validador->mailValido($data['emailFacebook']);

		$rol      = 	1;
		$status   = 	1;
		
		try {
		$usuario = new Default_Model_Usuario();
		$usuario->setEmail($emailFacebook);
		$usuario->setEmailnotificacion($emailNotificaciones);
		$usuario->setIdfacebook($data["idFacebook"]);
		$usuario->setNombre($nombre);
		$usuario->setUsername($username);
		$usuario->setRol($rol);
		$usuario->setStatus($status);
		//creditos gratuitos
		$usuario->setCreditos(500);
		$usuario->setCreatedAt();
		$usuario->setUpdatedAt(new DateTime("now"));
		
		
			$this->_em->persist($usuario);
			$this->_em->flush();
			return $usuario;
				
		} catch(Zend_Exception $e) {
			return false;
		}
		
		
	}
	
	
	
	/**
 	* Validacion de un usuario
 	* @param unknown $email
 	* @param unknown $userid
 	* @return boolean
 	*/
	public function userlogin( $usuario, $password){
		//instanciamos Zend_Auth
		$auth=Zend_Auth::getInstance();
		// sacar el adaptador
		$dbAdapter = $this->getAdapter();
		//Creamos el adaptador para la autentificación por base de datos
		$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter,'Usuario');
		//asignamos las columnas de autenticacion
		$authAdapter->setIdentityColumn('username')->setCredentialColumn('password')
		->setIdentity($usuario)->setCredential( hash('whirlpool', $password) );
		//autenticamos y obtenemos el objeto resultante
		$validar = $auth->authenticate($authAdapter);
	
		if ($validar->isValid()) {
			$usuario	=	$this->_em->getRepository("Default_Model_Usuario")->findBy(array("username"=>$usuario));
			$usuario=$usuario[0];
			$usuario->setUpdatedAt(new DateTime("now"));
			$this->_em->persist($usuario);
			$this->_em->flush();
			//guardamos la informacion de los campos de la tabla autenticada para la sesión
			$userInfo = $authAdapter->getResultRowObject(null,'password');
			$authStorge = $auth->getStorage();
			$authStorge->write($userInfo);
			return true;
		} else {
			return false;
		}
	}
	
	
}