<?php 
defined('CRON_PATH')
|| define('CRON_PATH', realpath(dirname(__FILE__) ));

require_once(CRON_PATH."/../library/conektaSDK/Conekta.php");
require_once ("correosMandrill.php");
require_once(CRON_PATH."/../library/facebookSDK/facebook.php");

class inboxCheck {
	
	private $_mysql;
	private $_desarrollo=false;
	private $_facebook;
	
	
	public function conectarbase(){
		if($this->_desarrollo)
			$this->_mysql	=	new mysqli("127.0.0.1", "root", "root", "facebookespia");
		else
			$this->_mysql	=	new mysqli("facebookespia.db.9982935.hostedresource.com", "facebookespia", "J6wpg231@", "facebookespia");
	}	
	
	
	public function __construct(){
		$this->conectarbase();
		
		if($this->_mysql->connect_errno){
				throw new Exception("Error al conectar la base ".$this->_mysql->error);
		}
	}
	
	/**
	 * Devuelve todas las victimas 
	 */
	public function sendMail(){
		
		$now 	=	date('Y-m-d',strtotime("-1 days"));
		$hoy 		=	($now." 00:10");
		$sql	=	"	
					SELECT 		con.victima_id,vic.idFacebook,vic.nombre, max(men.fechaMensaje) as ultimoMensaje 
					FROM 		facebookespia.Conversacion con
					INNER JOIN 	Mensaje men 
						ON 		con.id = men.conversacion_id
					INNER JOIN	Victima vic
						ON 		con.victima_id = vic.id
					GROUP BY	con.victima_id,vic.idFacebook,vic.nombre
					HAVING		max(men.fechaMensaje) > '{$hoy}'
				";
		
		$victimasMensajes		=		$this->_mysql->query($sql);
		
		$users					=	array();
		$correos				=	array();
		
		while($victima	=	$victimasMensajes->fetch_assoc()){
			$nombreVictima 		=  	$victima["nombre"];
			$idVictima			=	$victima["victima_id"];
			$idFacebookVictima	=	$victima["idFacebook"];
			$sql  = "
						SELECT 		u.nombre,u.idFacebook,u.email 
						FROM 		Anzuelo anz
						INNER JOIN 	Usuario u 
						ON			u.id = anz.usuario_id
						WHERE		victima_id = {$idVictima} ";
					
						$usuarios	=		$this->_mysql->query($sql);
						while($usuario	=	$usuarios->fetch_assoc()){
							if(!in_array($usuario["idFacebook"],$users)){
									$users[] = $usuario["idFacebook"];
									$correos[$usuario["email"]]["nombre"]=$usuario["nombre"];
									$correos[$usuario["email"]]["idFacebook"]=$usuario["idFacebook"];
									$correos[$usuario["email"]]["email"]=$usuario["email"];
									$correos[$usuario["email"]]["victimas"][]=array("nombre"=>$victima["nombre"],
																					"idFacebook"=>$victima["idFacebook"]
																					);
							}else{
								$correos[$usuario["email"]]["victimas"][]=array("nombre"=>$victima["nombre"],
										"idFacebook"=>$victima["idFacebook"]
								);
							}
						}	
		}
		
		
		$mailer 	=  new correosMandrill();
		foreach($correos as $correo){
			
			$html="";
			foreach($correo["victimas"] as $victima){
				$html .= "<img src='http://graph.facebook.com/{$victima["idFacebook"]}/picture?type=square' style='margin-right:50px;'>
						<strong>{$victima["nombre"]}</strong>
						<br>";
			}
			
			$parametros["email"]	=	$correo["email"];
			$parametros["nombreUsuario"]	=	$correo["nombre"];
			$parametros["victimas"]	=	$html;

			$mailer->enviarMail($parametros);
		}
		
		echo "se enviaron ".count($correos)." correos";
	}
		
}

$inbox	= 	new inboxCheck();
$inbox->sendMail();
