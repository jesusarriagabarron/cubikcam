<?php

defined('CRON_PATH')
|| define('CRON_PATH', realpath(dirname(__FILE__) ));

require_once(CRON_PATH."/../library/mandrillSDK/Mandrill.php");

class correosMandrill{

	private $_mandrill;
	private $_twitter;
	private $_facebook;
	private $_config;


	public function __construct(){
		//iniciamos mandrill
		$this->_mandrill = new Mandrill("7a_vHPi_REGrJ4st8aGe7w");
		$this->_facebook	=	"https://www.facebook.com/";
		$this->_twitter		=	"https://www.facebook.com/";

	}
	
	/**
	 * Crea y envía un correo de confirmación de recepción de pago.
	 * @param Default_Model_Usuario $usuario
	 */
	public function enviarMail($parametros){
		//Datos generales
		$this->_config["emailDestino"]  = 	$parametros["email"];
		$this->_config["nombreDestino"] =	$parametros["nombreUsuario"];
		$this->_config["victimas"]		=	$parametros["victimas"];	
		$this->_config["identificador"]	=   "facebookespia-nuevasCapturas";
		$this->_config["titulo"] = "Nuevas conversaciones capturadas de facebook";

		//Mensaje en HTML
		$this->_config["mensajeHTML"]="<p style='text-align:justify;'>
					Te informamos que tenemos capturados nuevas charlas INBOX de facebook del usuario <br><br>
						".$this->_config["victimas"]."

						Entra aquí para revisarlas ahora  <a href='http://www.endaroo.com' target='_blank'>FacebookESPIA</a>
										
				</p>";
		$this->sendTemplate();
	}
	
	
	public function sendTemplate(){
		$f=new DateTime('now');
		
		$template_name= "facebookespia-nuevascapturas";
		$template_content = array(
				array(
						"name" => "example name",
						"content" => "example content"
				)
		);
		$message = array(
				"subject"		=>	$this->_config["titulo"],
				"from_email"	=>	"no.reply@endaroo.com",
				"from_name"		=>	"FacebookEspia",
				"to"			=>	array(
										array(
										"email"		=>		$this->_config["emailDestino"] ,
										"name"		=>		$this->_config["nombreDestino"] ,
										"type"		=>		"to"
									 	)
	        					  	),
				"important"		=>	false,
				"track_opens" => null,
				"track_clicks" => null,
				"auto_text" => null,
				"auto_html" => null,
				"inline_css" => null,
				"url_strip_qs" => null,
				"preserve_recipients" => null,
				"view_content_link" => null,
				"tracking_domain" => null,
				"signing_domain" => null,
				"return_path_domain" => null,
				"merge" => true,
				"tags" => array($this->_config["identificador"]),
				"global_merge_vars" => array(
						array("LIST:COMPANY"				=> "facebookespia")
				),
				"merge_vars" => array(
						array(
								"rcpt" => $this->_config["emailDestino"] ,
								"vars" => array(
										array(
												"name" => "COMPRADOR",
												"content" => $this->_config["nombreDestino"] 
										),
										array(
												"name" => "COMPANY",
												"content" => "MegaquinielasBrasil2014"
										),
										array(
												"name" => "CURRENT_YEAR",
												"content" => $f->format('Y')  
										),
										array(
												"name"		=>	"MENSAJE",
												"content"	=>	$this->_config["mensajeHTML"]
										),
										array(	"name"	=>	"TWITTER","content"	=>	$this->_twitter	),
										array(	"name"	=>	"FACEBOOK","content"	=>	$this->_facebook )
								)
						)
				),
				"recipient_metadata" => array(
						array(
								"rcpt" => $this->_config["emailDestino"] ,
								"values" => array("user_id" => $this->_config["nombreDestino"] )
						)
				),
		);// Fin array message
				
		$async = false;
		$ip_pool = "Main Pool";
		$send_at = date("Y-m-d hh:ii:ss",strtotime("-1 day"));
		try{
			$result = $this->_mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool);
		} catch(Mandrill_Error $e) {
		
		}
	}
}
